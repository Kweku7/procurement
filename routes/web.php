<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('about', function () {
    return view('about');
})->name('about');

Route::get('services/offices', function () {
    return view('office');
})->name('office');

Route::get('services/domestic', function () {
    return view('domestic');
})->name('domestic');

Route::get('services/groceries', function () {
    return view('groceries');
})->name('groceries');

Route::get('contact', function () {
    return view('contact');
})->name('contact');



