@extends('boilers.app')

@section('content')

    <div class="hero-area">
        <div class="page-header parallax" style="background-image:url({{ asset('vestige/images/about_banner.jpg') }})"><div><div><span>About Us</span></div></div></div>
    </div>
    <!-- Notive Bar -->
    <div class="notice-bar">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">About</li>
            </ol>
        </div>
    </div>
    <!-- Start Body Content -->
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="short accent-color">NEOWEC Logistics and Procurement</h4>
                        <h1>About NEOWEC L&P</h1>
                        <p class="lead">With NEOWEC on your side be rest assured that all logistics and procurement processes would be taken care of without any glitch. The team at NEOWEC are qualified and
                            experienced and we pride ourselves in the satisfaction of knowing that great
                            customer service and value is being delivered to you.</p>
                    </div>
                </div>

                <hr class="fw">
                <h1>Why you choose us</h1>
                <div class="spacer-40"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="icon-box ibox-border">
                            <div class="ibox-icon">
                                <i class="fa fa-gear"></i>
                            </div>
                            <h3>Materials Procurement</h3>
                            <p>
                                NEOWEC's Procurement and Logistics procures materials for individuals or companies from a host of established and
                                existing suppliers. Our efforts
                                are targeted at finding quality products yet cost-effective, with on-time delivery anywhere at anytime.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="icon-box ibox-border">
                            <div class="ibox-icon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <h3>Logistics</h3>
                            <p>NEOWEC's Procurement and Logistics team oversees the delivery of all materials(shipment).
                                This includes ensuring proper control of the materials to the point of embarkation, negotiating shipping
                                contracts, negotiating freight rates, operating the cargo terminal, and overseeing compliance regulations worldwide.</p>
                        </div>
                    </div>
                </div>
                <div class="spacer-60 hidden-xs hidden-sm"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="icon-box ibox-border">
                            <div class="ibox-icon">
                                <i class="fa fa-file"></i>
                            </div>
                            <h3>Contracts</h3>
                            <p>
                                Our contractoral services includes various types of technical agreements for engineering, project management,
                                and downstream research. The scope of services being procured also extends to other areas, including public affairs,
                                career development, and professional consultation relating to finance and human resources.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="icon-box ibox-border">
                            <div class="ibox-icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <h3>Our Sourcing is strategic</h3>
                            <p>Strategic Sourcing is key to the procurement process which adds value by improving and reevaluating the purchasing activities. We work with major suppliers which in turn aid in assessing the market, identifying new suppliers and developing better sourcing strategies that focus on demand and supply issues, and minimizing risks and costs.</p>
                        </div>
                    </div>
                </div>

                <div class="spacer-20"></div>
            </div>
        </div>
    </div>
    <!-- End Body Content -->

@endsection