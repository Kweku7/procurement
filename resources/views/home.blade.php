@extends('boilers.app')

@section('content')
    <div class="hero-area">
        <!-- Start Hero Slider -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>
        <div class="slider-rev-cont">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul style="display:none;">
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000"  data-saveperformance="off"  data-title="Slide 1">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('vestige/images/logistics.jpg') }}"  alt="fullslide1"  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption light_heavy_60 largewhitebg sfb rtt tp-resizeme"
                                 data-x="left" data-hoffset="20"
                                 data-y="center" data-voffset="0"
                                 data-speed="600"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">Neowec Logistics and Procurement
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption light_medium_30_shadowed medium_bg_darkblue sfb rtt tp-resizeme"
                                 data-x="left" data-hoffset="20"
                                 data-y="center" data-voffset="70"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">You want it, you get it
                            </div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000"  data-saveperformance="off"  data-title="Slide 2">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('vestige/images/office.png') }}"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption largeblackbg sfb rtt tp-resizeme"
                                 data-x="left" data-hoffset="20"
                                 data-y="bottom" data-voffset="-80"
                                 data-speed="600"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">Quality and Cost Effective
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption light_medium_20_shadowed modern_big_bluebg sfb rtt tp-resizeme"
                                 data-x="left" data-hoffset="20"
                                 data-y="bottom" data-voffset="-50"
                                 data-speed="600"
                                 data-start="900"
                                 data-easing="Power4.easeOut"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">all from Neowec limiited!
                            </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer" style="display:none;"></div>
                </div>
            </div>
        </div>
        <!-- End Hero Slider -->
    </div>
    <!-- Notive Bar -->
    <div class="notice-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10">
                    <p class="lead" style="font-size: medium!important;">Complete an easy form and we will get back to you with a quote to show you how easy it is to have us with you</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <a href="#" class="btn btn-primary">REQUEST A QUOTE</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Body Content -->
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h3 class="widget-title">WHAT WE DO</h3>
                        <p>
                            We are a group of people who are dedicated and pationate about helping your business grow by taking care of all logistics and procuring process. With this all you have to do is relax and focus more on the other aspects of your business.
                            <i>You want it, You get it...</i>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="featured-block">
                            <figure>
                                <a href="{{ route('office') }}">
                                    <img src="{{ asset('vestige/images/office2.jpg') }}" class="m1" alt="Office Equipment">
                                    <span class="caption">Office Equipment</span>
                                </a>
                            </figure>
                            <p>The Vestige, museum of antiquity has a variety of year-round internship programs available for students of all ages.</p>
                            <a href="{{ route('office') }}" class="basic-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="featured-block">
                            <figure class="m2">
                                <a href="{{ route('domestic') }}">
                                    <img src="{{ asset('vestige/images/domestic2.jpg') }}" alt="">
                                    <span class="caption">Domestic Eqpmt.</span>
                                </a>
                            </figure>
                            <p>Our memberships provide wonderful benefits and supports our goal to preserve and conserve our local history.</p>
                            <a href="{{ route('domestic') }}" class="basic-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="featured-block">
                            <figure style="">
                                <a href="{{ route('groceries') }}">
                                    <img src="{{ asset('vestige/images/groc2.jpg') }}" alt="">
                                    <span class="caption">Groceries</span>
                                </a>
                            </figure>
                            <p>Our beautiful historic properties are the ideal year-round location to host your wedding or special event.</p>
                            <a href="{{ route('groceries') }}" class="basic-link">Read More</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="spacer-50"></div>
            <div class="dgray-bg">
                <div class="skewed-title-bar">
                    <div class="container">
                        <h4>
                            <span>NEOWEC L & P</span>
                        </h4>
                    </div>
                </div>
                <div class="padding-tb45">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 class="light-title">Our guiding principle</h2>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <ul class="owl-carousel carousel-fw" id="testimonials-slider" data-columns="2" data-autoplay="" data-pagination="no" data-arrows="yes" data-single-item="no" data-items-desktop="2" data-items-desktop-small="3" data-items-tablet="1" data-items-mobile="1">
                                        <li class="item">
                                            <blockquote>
                                                <p>Neowec has the art and mastery of saving money yet giving the best logistics and solutions for an optimum production. </p>
                                                <cite>Quality And Cost Efficiency</cite>
                                            </blockquote>
                                        </li>
                                        <li class="item">
                                            <blockquote>
                                                <p>Our dedicated team of experienced and knowledgeable staff offer a personalised approach to provide a satisfactory service which we believe gives customer the power to grow to the optimum.</p>
                                                <cite>Customer Satisfactory</cite>
                                            </blockquote>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Body Content -->
@endsection