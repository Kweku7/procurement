@extends('boilers.app')

@section('content')

    <div class="hero-area">
        <div class="page-header parallax" style="background-image:url({{ asset('vestige/images/dom_banner.jpg') }}"><div><div><span>Domestic</span></div></div></div>
    </div>
    <!-- Notive Bar -->
    <div class="notice-bar">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Services</a></li>
                <li class="active">Domestic</li>
            </ol>
        </div>
    </div>
    <!-- Start Body Content -->
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="accent-color short">Domestic Commodities</h4>
                        <div class="spacer-10"></div>
                        <h5 style="font-size: medium">We are a dedicated procurement services provider of domestic equipment. We work with you to identify opportunities to provide quality, effective and
                            affordable domestic equipment. We help our clients achieve step-change improvements through our combined approach of delivering
                            immediate cost savings at the same time benefiting through smart and strategic procurement. For quality, cost-effective and energy efficient:</h5>
                        <div class="spacer-30"></div>
                    </div>
                </div>
            </div>
            <div class="spacer-30"></div>
            <div class="lgray-bg padding-tb45 margin-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="icon-box">
                                <div class="ibox-icon">
                                    <i class="fa fa-truck"></i>
                                </div>
                                <h3>Home Appliance Supplies And Servicing</h3>
                                <p>
                                <ul class="carets">
                                    <li>Air Conditioning </li>
                                    <li>Furniture </li>
                                    <li>Stereo</li>
                                    <li>Internet Connection</li>
                                    <li>Televisions</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon-box">
                                <div class="ibox-icon">
                                    <i class="fa fa-coffee"></i>
                                </div>
                                <h3>Kitchen ware</h3>
                                <p>
                                <ul class="carets">
                                    <li>Sinks</li>
                                    <li>Dishes</li>
                                    <li>Dish Washers</li>
                                    <li>Laundry</li>
                                    <li>Cleaners</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon-box">
                                <div class="ibox-icon">
                                    <i class="fa fa-user-secret"></i>
                                </div>
                                <h3>Security</h3>
                                <p>
                                <ul class="carets">
                                    <li>Cameras </li>
                                    <li>Security Personel</li>
                                    <li>Security Dogs</li>
                                    <li>Tracking Systems</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="text-align-center padding-tb20">
                    <h2 class="accent-color">Request A Quote</h2>
                    <h4>Complete an easy form and we will get back to you with a quote to show you how easy it is to have us with you!</h4>
                    <a href="#" class="btn btn-primary">Request Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Body Content -->

@endsection