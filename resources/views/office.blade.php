@extends('boilers.app')

@section('content')

    <div class="hero-area">
        <div class="page-header parallax" style="background-image:url({{ asset('vestige/images/office_banner.jpg') }}"><div><div><span>Offices</span></div></div></div>
    </div>
    <!-- Notive Bar -->
    <div class="notice-bar">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Services</a></li>
                <li class="active">Offices</li>
            </ol>
        </div>
    </div>
    <!-- Start Body Content -->
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="accent-color short">Office Commodities</h4>
                        <h2>NEOWEC provides a comprehensive cost reduction and quality service within the following commodities:</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="icon-box">
                                    <div class="ibox-icon">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                    <h3>Equipment Supplies And Services</h3>
                                    <p>We provide and deliver onsite office equipment and the services of these equipment.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="icon-box">
                                    <div class="ibox-icon">
                                        <i class="fa fa-user-secret"></i>
                                    </div>
                                    <h3>Corporate Security Services</h3>
                                    <p>We understand the security needs of business, thus we provide security including cyber.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="icon-box">
                                    <div class="ibox-icon">
                                        <i class="fa fa-object-group"></i>
                                    </div>
                                    <h3>Members and Facilities Management</h3>
                                    <p>We provide insurrance among other staff management strategies to help boost your business.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-30"></div>
            <div class="lgray-bg padding-tb45 margin-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h2>Commodity List</h2>
                            <div class="fw-video">
                                <img src="{{ asset('vestige/images/office_list.jpg') }}">
                            </div>
                            <div class="spacer-40 visible-sm visible-xs"></div>
                        </div>

                        <div class="col-md-9">
                            <div class="spacer-50"></div>
                            <div  class="row">
                                <div class="col-md-4">
                                    <ul class="carets">
                                        <li>Air Conditioning </li>
                                        <li>Card Services & Terminals</li>
                                        <li>Cash & Security Handling</li>
                                        <li>Corporate Banking</li>
                                        <li>Facilities Management</li>
                                        <li>Security</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul class="carets">
                                        <li>Office Supplies & Furniture</li>
                                        <li>Personal Protective Equipment</li>
                                        <li>Secure Archive Storage</li>
                                        <li>Supplier Consolidation</li>
                                        <li>Retail Consumables</li>
                                        <li>Waste Management & Recycling</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul class="carets">
                                        <li>Network Printers, Photocopiers & MFD's</li>
                                        <li>I.T Equipment & Services</li>
                                        <li>Fleet Hire & Leasing</li>
                                        <li>Logistics & Warehousing</li>
                                        <li>Marketing</li>
                                    </ul>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="text-align-center padding-tb20">
                    <h2 class="accent-color">Request A Quote</h2>
                    <h4>Complete an easy form and we will get back to you with a quote to show you how easy it is to have us with you!</h4>
                    <a href="#" class="btn btn-primary">Request Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Body Content -->

@endsection