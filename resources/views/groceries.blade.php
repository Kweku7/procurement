@extends('boilers.app')

@section('content')

    <div class="hero-area">
        <div class="page-header parallax" style="background-image:url({{ asset('vestige/images/dom_banner.jpg') }}"><div><div><span>Foodstuff And Groceries</span></div></div></div>
    </div>
    <!-- Notive Bar -->
    <div class="notice-bar">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Services</a></li>
                <li class="active">Foodstuff And Groceries</li>
            </ol>
        </div>
    </div>
    <!-- Start Body Content -->
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="accent-color short">Foodstuff And Groceries</h4>
                        <div class="spacer-10"></div>
                        <h5 style="font-size: medium">With our special negotiation ability, NEOWEC helps you purchase your groceries and foodstuff with quality in mind and deliver it to you when
                            and where ever you might be. We are establised to take all your stress and and save you time while you get the best to keep healthy. We serve individuals and groups too.. </h5>
                        <div class="spacer-10"></div>
                    </div>
                </div>
            </div>
            <div class="spacer-30"></div>
            <div class="lgray-bg padding-tb45 margin-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2>Commodity List</h2>
                            <div class="fw-video">
                                <img src="{{ asset('vestige/images/grocery_list.png') }}">
                            </div>
                            <div class="spacer-40 visible-sm visible-xs"></div>
                        </div>

                        <div class="col-md-8">
                            <div class="spacer-50"></div>
                            <div  class="row">
                                <div class="col-md-4">
                                    <ul class="carets">
                                        <li>Canned Food </li>
                                        <li>Frozen foods</li>
                                        <li>Sea Foods</li>
                                        <li>Non-pershables</li>
                                        <li>Diary foods</li>
                                        <li>And many More...</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="text-align-center padding-tb20">
                    <h2 class="accent-color">Request A Quote</h2>
                    <h4>Complete an easy form and we will get back to you with a quote to show you how easy it is to have us with you!</h4>
                    <a href="#" class="btn btn-primary">Request Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Body Content -->

@endsection