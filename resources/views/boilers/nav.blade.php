<ul class="sf-menu">
    <li><a href="{{ route('home') }}">Home</a></li>

    <li><a href="{{ route('about') }}">About</a> </li>

    <li><a href="javascript:void(0);">Services</a>
        <ul class="dropdown">
            <li><a href="{{ route('office') }}">Office Equipment</a></li>
            <li><a href="{{ route('domestic') }}">Domestic Equipment</a></li>
            <li><a href="{{ route('groceries') }}">Groceries & Foodstuff</a></li>
        </ul>
    </li>
    <li><a href="{{ route('contact') }}">Contact</a></li>
</ul>