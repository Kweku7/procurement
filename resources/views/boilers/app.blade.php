<!DOCTYPE HTML>
<html class="no-js">
<head>
    <!-- Basic Page Needs
      ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>NEOWEC | LOGISTICS & PROCUREMENT</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <!-- CSS
      ================================================== -->
    <link href="{{ asset('vestige/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vestige/css/bootstrap-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vestige/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vestige/vendor/prettyphoto/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vestige/vendor/owl-carousel/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vestige/vendor/owl-carousel/css/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="{{ asset('vestige/css/ie.css') }}" media="screen" /><![endif]-->
    <link href="{{ asset('vestige/css/custom.css') }}" rel="stylesheet" type="text/css"><!-- CUSTOM STYLESHEET FOR STYLING -->

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vestige/css/extralayers.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vestige/vendor/revslider/css/settings.css') }}" media="screen" />

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
    <!-- Color Style -->
    <link class="alt" href="{{ asset('vestige/colors/color1.css') }}" rel="stylesheet" type="text/css">
    <!-- SCRIPTS
      ================================================== -->
    <script src="{{ asset('vestige/js/modernizr.js') }}"></script><!-- Modernizr -->
</head>
<body class="home header-style2">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->
<div class="body">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6  col-sm-6">
                    <p>Neowec! You want it, you get it!!!</p>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="pull-right social-icons-colored">
                        <li class="facebook"><a href="javascript:void(0)"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="twitter"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                        <li class="instagram"><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                        <li class="flickr"><a href="javascript:void(0)"><i class="fa fa-flickr"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Site Header -->
    <div class="site-header-wrapper">
        <header class="site-header">
            <div class="container sp-cont">
                <div class="site-logo">
                    <h1><a href="//neowec.com"><img src="{{ asset('vestige/images/logo.png') }}" alt="Logo"></a></h1>
                </div>
                <a href="#" class="visible-sm visible-xs" id="menu-toggle"><i class="fa fa-bars"></i></a>
                <!-- Main Navigation -->
                <nav class="main-navigation dd-menu toggle-menu" role="navigation">
                    @include('boilers.nav')
                </nav>
            </div>
        </header>
        <!-- End Site Header -->
    </div>

    @yield('content')

    <!-- Start site footer -->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="widget footer-widget">
                        <h4 class="widget-title">Contact Info</h4>
                        <address>
                            <a href="javascript:void(0)"><strong>Location</strong></a><br>
                            <span>Nelson Palazzio Opp. Appostolic Junction <br>
                                Sakumono Village Tema</span>
                        </address>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">

                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="widget footer-widget">
                        <h4 class="widget-title hidden-sm hidden-xs"><br></h4>
                        <address>
                            <a href="javascript:void(0)"><strong>Phones</strong></a><br>
                            (030) 340 2416 / (020) 060 4343 / (024) 141 6266 <br>
                            Email: <a href="mailto:info@neowec.com">info@neowec.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <footer class="site-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 copyrights-left">
                    <p>&copy;{{ date('Y') }} Neowec Limited.</p>
                </div>
                <div class="col-md-6 col-sm-6 copyrights-right">
                    <ul class="pull-right social-icons-colored">
                        <li class="facebook"><a href="javascript:void(0)"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="twitter"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                        <li class="instagram"><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                        <li class="flickr"><a href="javascript:void(0)"><i class="fa fa-flickr"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- End site footer -->
    <a id="back-to-top"><i class="fa fa-chevron-up"></i></a>
</div>
<script src="{{ asset('vestige/js/jquery-2.1.3.min.js') }}"></script> <!-- Jquery Library Call -->
<script src="{{ asset('vestige/vendor/prettyphoto/js/prettyphoto.js') }}"></script> <!-- PrettyPhoto Plugin -->
<script src="{{ asset('vestige/js/ui-plugins.js') }}"></script> <!-- UI Plugins -->
<script src="{{ asset('vestige/js/helper-plugins.js') }}"></script> <!-- Helper Plugins -->
<script src="{{ asset('vestige/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script> <!-- Owl Carousel -->
<script src="{{ asset('vestige/js/bootstrap.js') }}"></script> <!-- UI -->
<script src="{{ asset('vestige/js/init.js') }}"></script> <!-- All Scripts -->
<script src="{{ asset('vestige/vendor/flexslider/js/jquery.flexslider.js') }}"></script> <!-- FlexSlider -->

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="{{ asset('vestige/vendor/revslider/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vestige/vendor/revslider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay:"none",
                delay:9000,
                startwidth:1100,
                startheight:500,
                hideThumbs:200,

                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:5,

                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"preview2",

                touchenabled:"on",
                onHoverStop:"on",

                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,


                keyboardNavigation:"on",

                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,

                shadow:0,
                fullWidth:"on",
                fullScreen:"off",

                spinner:"spinner0",

                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,

                shuffle:"off",

                autoHeight:"off",
                forceFullWidth:"off",



                hideThumbsOnMobile:"off",
                hideNavDelayOnMobile:1500,
                hideBulletsOnMobile:"off",
                hideArrowsOnMobile:"off",
                hideThumbsUnderResolution:0,

                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0
            });
    });	//ready
</script>
</body>
</html>